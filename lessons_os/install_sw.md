Установка пакета:
sudo apt-get install <название пакета>

Удаление пакета:
sudo apt-get purge --auto-remove <название пакета>

Обновление ОС:
sudo apt-get update 
sudo apt-get upgrade